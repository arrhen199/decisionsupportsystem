﻿using Logic;
using Logic.Algorithms;
using Microsoft.Win32;
using SWD.Dialogs;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //says which column's context menu was opened
        private string LastClickedColumnHeader { get; set; }
        private Controller Controller { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Controller = new Controller();
        }

        private void ColumnHeader_ContextMenuOpening(object sender, RoutedEventArgs e)
        {
            var columnHeader = sender as DataGridColumnHeader;
            if (columnHeader != null)
            {
                LastClickedColumnHeader = columnHeader.Content.ToString();
                int index = Controller.Table.GetColumnIndex(LastClickedColumnHeader);
                var type = Controller.Table.GetColumnType(index);

                List<int> onlyString = new List<int> { 0 };
                List<int> onlyNumeric = new List<int> { 1, 2 };


                onlyNumeric.ForEach(x => ((MenuItem)columnHeader.ContextMenu.Items[x]).Visibility = Visibility.Collapsed);
                onlyString.ForEach(x => ((MenuItem)columnHeader.ContextMenu.Items[x]).Visibility = Visibility.Collapsed);

                List<int> clickedOptionCollection = type == "string" ? onlyString : onlyNumeric;
                clickedOptionCollection.ForEach(x => ((MenuItem)columnHeader.ContextMenu.Items[x]).Visibility = Visibility.Visible);
            }
        }

        private void MenuItemLoadFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            //dlg.DefaultExt = ".png";
            //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                string extension = System.IO.Path.GetExtension(filename);
                if (string.Equals(extension, ".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    ExcelSheetDialog dialog = new ExcelSheetDialog();
                    dialog.SetFilePath(filename);
                    if (dialog.ShowDialog() == true)
                    {
                        Controller.LoadData(filename, dialog.SheetName);
                    }
                }
                else
                {
                    Controller.LoadData(filename);
                }

                UpdateTable();
            }

        }

        private void UpdateTable()
        {
            DataView dv = new DataView(Controller.Table.GetTable());
            DataGridTable.ItemsSource = dv;
        }

        private void MenuItemDiscretization_Click(object sender, RoutedEventArgs e)
        {
            DiscretizationDialog dialog = new DiscretizationDialog();
            if (dialog.ShowDialog() == true)
            {
                int number = dialog.DiscretizationSteps;
                bool deleteColumn = dialog.DeleteColumn;
                SimpleAlgorithmsInvoker.Discretization(Controller.Table, Controller.Table.GetColumnIndex(LastClickedColumnHeader), number, deleteColumn);
                UpdateTable();
            }
        }

        private void MenuItemNormalization_Click(object sender, RoutedEventArgs e)
        {
            NormalizationDialog dialog = new NormalizationDialog();
            if (dialog.ShowDialog() == true)
            {
                bool deleteColumn = dialog.DeleteColumn;
                SimpleAlgorithmsInvoker.Normalization(Controller.Table, Controller.Table.GetColumnIndex(LastClickedColumnHeader), deleteColumn);
                UpdateTable();
            }
        }

        private void MenuItemToInt_Click(object sender, RoutedEventArgs e)
        {
            ChangeToIntDialog dialog = new ChangeToIntDialog();
            if (dialog.ShowDialog() == true)
            {
                bool deleteColumn = dialog.DeleteColumn;
                SimpleAlgorithmsInvoker.StringsToInts(Controller.Table,
                    Controller.Table.GetColumnIndex(LastClickedColumnHeader), dialog.OrderAlphabetically, deleteColumn);
                UpdateTable();
            }


        }

        private void MenuItemSaveFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt";
            if (saveFileDialog.ShowDialog() == true)
                Controller.SaveData(saveFileDialog.FileName);
        }

        private void MenuItemCharts_Click(object sender, RoutedEventArgs e)
        {
            if (Controller.Table == null)
            {
                MessageBox.Show("Load data first!");
                return;
            }

            ChartsChoiceDialog dialog = new ChartsChoiceDialog();
            dialog.Table = Controller.Table;

            if (dialog.ShowDialog() == true)
            {
                if (dialog.Is2D)
                {
                    if (dialog.ClassChecked)
                    {
                        _2DChartWindow chart = new _2DChartWindow(dialog.ColumnAxisX, dialog.ColumnAxisY, dialog.ColumnClass, Controller.Table);
                        chart.Show();
                    }
                    else
                    {
                        _2DChartWindow chart = new _2DChartWindow(dialog.ColumnAxisX, dialog.ColumnAxisY, Controller.Table);
                        chart.Show();

                    }
                }
                else
                {
                    //3d
                    //dialog.ColumnAxisX, dialog.ColumnAxisY, dialog.ColumnAxisZ
                }
            }
        }
        private void MenuItemKnnQuality_Click(object sender, RoutedEventArgs e)
        {
            if (Controller.Table == null)
            {
                MessageBox.Show("Load data first!");
                return;
            }

            KnnQualityDialog dialog = new KnnQualityDialog();
            dialog.Table = Controller.Table;

            if (dialog.ShowDialog() == true)
            {

            }

        }

        private void MenuItemKmeans_Click(object sender, RoutedEventArgs e)
        {
            if (Controller.Table == null)
            {
                MessageBox.Show("Load data first!");
                return;
            }

            KmeansDialog dialog = new KmeansDialog();
            var table = Controller.Table.GetTable().Copy();
            table.Columns.RemoveAt(table.Columns.Count - 1);
            var tableWithoutLastColumn = new SWDTable(table);
            dialog.Table = tableWithoutLastColumn;

            dialog.Controller = Controller;

            if (dialog.ShowDialog() == true)
            {

            }

        }

        private void MenuItemDecisionTree_Click(object sender, RoutedEventArgs e)
        {
            DecisionTreeDialog dialog = new DecisionTreeDialog();
            dialog.Table = (SWDTable)Controller.Table;

            dialog.ShowDialog();
        }

        private void MenuItemSeparation_Click(object sender, RoutedEventArgs e)
        {
            SeparationDialog dialog = new SeparationDialog();
            dialog.Table = (SWDTable)Controller.Table;
            dialog.Controller = Controller;
            dialog.Show();
        }
    }
}
