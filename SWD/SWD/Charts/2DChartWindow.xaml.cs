﻿using Logic;
using Logic.Algorithms.Classifications;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD
{
    /// <summary>
    /// Interaction logic for _2DChartWindow.xaml
    /// </summary>
    public partial class _2DChartWindow : Window
    {
        private List<Brush> MyBrushes { get; set; }
        private int BrushNumber { get; set; }

        public _2DChartWindow(string columnAxisX, string columnAxisY, ISWDTable table)
        {
            InitializeComponent();
            initBrushes();

            setAxisTitles(columnAxisX, columnAxisY);

            List<Point> list = new List<Point>();

            var xAxisValues = table.GetColumn(table.GetColumnIndex(columnAxisX));
            var yAxisValues = table.GetColumn(table.GetColumnIndex(columnAxisY));

            for (int i = 0; i < xAxisValues.Count(); i++)
            {
                list.Add(new Point(xAxisValues.ElementAt(i), yAxisValues.ElementAt(i)));
            }

            drawChart(list, columnAxisY);

        }

        public _2DChartWindow(string columnAxisX, string columnAxisY, string columnClass, ISWDTable table, List<SeparationLine> lines = null)
        {
            InitializeComponent();
            initBrushes();

            setAxisTitles(columnAxisX, columnAxisY);

            List<Point> list = new List<Point>();

            var xAxisValues = table.GetColumn(table.GetColumnIndex(columnAxisX));
            var yAxisValues = table.GetColumn(table.GetColumnIndex(columnAxisY));

            var classifiedPoints = setClasses(xAxisValues, yAxisValues, table, columnClass);
            foreach (KeyValuePair<string, List<Point>> item in classifiedPoints)
            {
                drawChart(item.Value, item.Key);
            }
            if(lines != null)
            {
                foreach (var line in lines)
                {
                    if(line.ColumnIndex == 0)
                    {
                        VerticalLine vline = new VerticalLine();
                        vline.Value = line.LineValue;
                        plotter.Children.Add(vline);
                    }
                    else
                    {
                        HorizontalLine hline = new HorizontalLine();
                        hline.Value = line.LineValue;
                        plotter.Children.Add(hline);
                    }
                    
                }
            }

        }

        private IDictionary<string, List<Point>> setClasses(IEnumerable<double> xAxisValues, IEnumerable<double> yAxisValues, ISWDTable table, string columnClass)
        {
            Dictionary<string, List<Point>> pointsMap = new Dictionary<string, List<Point>>();
            for (int i = 0; i < xAxisValues.Count(); i++)
            {
                var xValue = xAxisValues.ElementAt(i);
                var yValue = yAxisValues.ElementAt(i);
                var classValue = table.GetCell(i, table.GetColumnIndex(columnClass)).ToString();
                if (pointsMap.ContainsKey(classValue))
                {
                    pointsMap[classValue].Add(new Point(xValue, yValue));
                }
                else
                {
                    var points = new List<Point>();
                    points.Add(new Point(xValue, yValue));
                    pointsMap.Add(classValue, points);
                }
            }
            return pointsMap;
        }

        private void drawChart(List<Point> points, string legendTitle)
        {
            var points2 = new List<Point>() { points.First() };
            points.RemoveAt(0);
            

            var dataSource = new EnumerableDataSource<Point>(points2);
            dataSource.SetXMapping(x => x.X);
            dataSource.SetYMapping(y => y.Y);
            var brush = PickBrush();
            plotter.AddLineGraph(dataSource, new Pen(brush, 10), new CirclePointMarker
            {
                Size = 10,
                Fill = brush
            }, new PenDescription(legendTitle));

            var dataSource2 = new EnumerableDataSource<Point>(points);
            dataSource2.SetXMapping(x => x.X);
            dataSource2.SetYMapping(y => y.Y);
            
            plotter.AddLineGraph(dataSource2, new Pen(Brushes.Transparent,0), new CirclePointMarker
            {
                Size = 10,
                Fill = brush
            }, new PenDescription(" "));
        }

        private void setAxisTitles(string columnAxisX, string columnAxisY)
        {
            var xTitle = (HorizontalAxisTitle)plotter.Children[10];
            xTitle.Content = columnAxisX;

            var yTitle = (VerticalAxisTitle)plotter.Children[11];
            yTitle.Content = columnAxisY;
        }

        private Brush PickBrush()
        {
            if (BrushNumber >= MyBrushes.Count)
                BrushNumber = 0;
            return MyBrushes[BrushNumber++];
        }

        private void initBrushes()
        {
            MyBrushes = new List<Brush>();
            MyBrushes.Add(Brushes.Red);
            MyBrushes.Add(Brushes.Black);
            MyBrushes.Add(Brushes.Blue);
            MyBrushes.Add(Brushes.Yellow);
            MyBrushes.Add(Brushes.Pink);
            MyBrushes.Add(Brushes.Plum);
            MyBrushes.Add(Brushes.Green);
            MyBrushes.Add(Brushes.Purple);           
            MyBrushes.Add(Brushes.Goldenrod);
            MyBrushes.Add(Brushes.DarkTurquoise);
            MyBrushes.Add(Brushes.DarkSalmon);
            MyBrushes.Add(Brushes.Brown);
            MyBrushes.Add(Brushes.Violet);
            MyBrushes.Add(Brushes.Orange);
            MyBrushes.Add(Brushes.PaleGreen);
            MyBrushes.Add(Brushes.Silver);
            MyBrushes.Add(Brushes.SkyBlue);
            MyBrushes.Add(Brushes.Gainsboro);
            
            BrushNumber = 0;
        }
    }
}
