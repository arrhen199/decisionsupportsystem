﻿using Logic.Algorithms.Clustering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for DifferenceMatrixDialog.xaml
    /// </summary>
    public partial class DifferenceMatrixDialog : Window
    {
        private DifferenceMatrix DifferenceMatrix;
        public DifferenceMatrixDialog(DifferenceMatrix matrix)
        {
            InitializeComponent();
            DifferenceMatrix = matrix;
            DataTable table = new DataTable();

            table.Columns.Add("ORIGINAL\\CLUSTERED");
            foreach (var col in matrix.Columns)
            {
                table.Columns.Add(col);
            }
            for(int i=0; i<matrix.Matrix.GetLength(0); i++)
            {
                object[] row = new object[matrix.Matrix.GetLength(0) + 1];
                row[0] = matrix.Columns[i];
                for (int j = 0; j < matrix.Matrix.GetLength(1); j++)
                {
                    row[j + 1] = matrix.Matrix[i, j].ToString();
                }
                table.Rows.Add(row);
            }

            DataView dv = new DataView(table);
            DataGridTable.ItemsSource = dv;

            LabelPurity.Content = String.Format("Correctly grouped: {0:0.00}%", matrix.GetPurityPercent()*100);
        }
    }
}
