﻿using Logic;
using Logic.Algorithms.Classifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for DecisionTreeDialog.xaml
    /// </summary>
    public partial class DecisionTreeDialog : Window
    {
        public SWDTable Table { get; set; }

        public DecisionTreeDialog()
        {
            InitializeComponent();
        }

        private void ButtonQuality_Click(object sender, RoutedEventArgs e)
        {
            int rowsCount = Table.GetTable().Rows.Count;
            var classificationResults = new List<KeyValuePair<String, String>>();
            for (int i = 0; i < rowsCount; i++)
            {
                var tableWithoutRow = Table.Copy();
                var row = tableWithoutRow.GetRow(i);
                string[] rowString = new string[row.Length - 1];
                for (int j = 0; j < row.Length-1; j++)
                {
                    rowString[j] = row[j].ToString();
                }

                tableWithoutRow.RemoveRow(i);

                var tree = DecisionTree.BuildTree(tableWithoutRow);
                var classify = DecisionTree.Classify(tableWithoutRow, tree, rowString);

                classificationResults.Add(new KeyValuePair<string, string>(row[row.Length - 1].ToString(), classify));
            }
            int correct = classificationResults.Count(x => x.Key == x.Value);
            MessageBox.Show(String.Format("Correctly classified: {0}%", 100*correct / rowsCount));
        }
    }
}
