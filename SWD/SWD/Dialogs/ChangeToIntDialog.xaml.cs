﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for ChangeToIntDialog.xaml
    /// </summary>
    public partial class ChangeToIntDialog : Window
    {
        public bool OrderAlphabetically { get; set; }
        public bool DeleteColumn { get; set; }
        public ChangeToIntDialog()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            OrderAlphabetically = RadioButtonAlph.IsChecked.Value;
            DeleteColumn = CheckBoxDeleteColumn.IsChecked.Value;
            Close();
        }
    }
}
