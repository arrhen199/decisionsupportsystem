﻿using Logic;
using Logic.Algorithms.Classifications;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for SeparationDialog.xaml
    /// </summary>
    public partial class SeparationDialog : Window
    {
        private ISWDTable _table;
        public Controller Controller { get; set; }
        public ISWDTable Table
        {
            get
            {
                return _table;
            }
            set
            {
                _table = value;
            }
        }

        public SeparationDialog()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, RoutedEventArgs e)
        {

            vectSizeResTxtblck.Text = Separation.GetVectors(Table).ToString();
            classDelCountTxtblock.Text = Separation.removedCount.ToString();
        }

        private void saveResButton_Click(object sender, RoutedEventArgs e)
        {

            vectSizeResTxtblck.Text = Separation.GetVectors(Table).ToString();
            Separation.GetBinaryData(Table);


                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "csv (*.csv)|*.csv";
                if (saveFileDialog.ShowDialog() == true)
                    Controller.SaveData(saveFileDialog.FileName, Separation.BinaryDataArray);
            }

        private void showLinesButton_Click(object sender, RoutedEventArgs e)
        {
            var columnCount = Table.GetTable().Columns.Count;
            if (columnCount > 3)
            {
                MessageBox.Show("Too many dimensions");
                return;
            }
            else if (Separation.SepLines == null)
            {
                Separation.GetVectors(Table);
            }
            else
            {
                var dimx = Table.GetColumnName(0);
                var dimy = Table.GetColumnName(1);
                var clas = Table.GetColumnName(2);
                _2DChartWindow chart = new _2DChartWindow(dimx, dimy, clas, Table, Separation.SepLines);
                chart.Show();
            }
        }

        private void classifyButton_Click(object sender, RoutedEventArgs e)
        {
            if (Separation.VectorToClassValueDict == null)
            {
                Separation.GetVectors(Table);
            }
            classResTxtblock.Text = "Class : " + Separation.GetClass(Separation.GetBinaryVector(classTxtbox.Text));
        }
    }
}
