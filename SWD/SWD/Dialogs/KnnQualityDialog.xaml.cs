﻿using Logic;
using Logic.Algorithms.Classifications;
using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for SimpleInformationDialog.xaml
    /// </summary>
    public partial class KnnQualityDialog : Window
    {
        public List<string> ColumnsNames { get; set; }
        public List<string> Metrics { get; set; }
        private ISWDTable _table;
        public ISWDTable Table
        {
            get
            {
                return _table;
            }
            set
            {
                _table = value;
                Init();
            }
        }

        private void Init()
        {
            for (int i = 0; i < Table.GetTable().Columns.Count; i++)
            {
                var colName = Table.GetTable().Columns[i].ColumnName;
                if (!colName.Contains("Classified"))
                {
                    ColumnsNames.Add(Table.GetTable().Columns[i].ColumnName);
                }
                
            }
            Metrics.Add(MetricsEnum.Euclid.ToString());
            Metrics.Add(MetricsEnum.Infinit.ToString());
            Metrics.Add(MetricsEnum.Manhattan.ToString());
            Metrics.Add(MetricsEnum.Mahalanobis.ToString());

            ComboBoxDecisionClasses.ItemsSource = ColumnsNames;
            ComboBoxMetrics.ItemsSource = Metrics;
        }

        public KnnQualityDialog()
        {
            InitializeComponent();
            Metrics = new List<string>();
            ColumnsNames = new List<string>();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            int k = 0;
            bool isKValueConverted = int.TryParse(TextBox_K.Text, out k);
            if (!isKValueConverted)
            {
                MessageBox.Show(" K should be integer value");
            }

            MetricsEnum selectedMetric;
            bool isMetricValueConverted = Enum.TryParse((string)ComboBoxMetrics.SelectedValue, out selectedMetric);

            if (!isMetricValueConverted)
            {
                MessageBox.Show(" Error parsing metric type");
            }
            if(isKValueConverted && isMetricValueConverted)
            {
                string knnResult = Knn.GetQuality(Table, k, selectedMetric, (string)ComboBoxDecisionClasses.SelectedValue);
                MessageBox.Show(string.Format("Quality of Knn is {0}", knnResult));
            }
            
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
