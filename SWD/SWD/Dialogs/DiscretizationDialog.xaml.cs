﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for DiscretizationDialog.xaml
    /// </summary>
    public partial class DiscretizationDialog : Window
    {
        public int DiscretizationSteps { get; set; }
        public bool DeleteColumn { get; set; }
        public DiscretizationDialog()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            int number = 1;
            bool converted = int.TryParse(TextBoxNumber.Text, out number);
            DiscretizationSteps = number;
            DeleteColumn = CheckBoxDeleteColumn.IsChecked.Value;
            DialogResult = true;
            Close();
        }

  

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void TextBoxNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
    }
}
