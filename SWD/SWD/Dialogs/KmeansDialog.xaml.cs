﻿using Logic;
using Logic.Algorithms.Clustering;
using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for KmeansDialog.xaml
    /// </summary>
    public partial class KmeansDialog : Window
    {
        public List<string> Metrics { get; set; }
        public Controller Controller { get; set; }
        private ISWDTable _table;
        public ISWDTable Table
        {
            get
            {
                return _table;
            }
            set
            {
                _table = value;
                Init();
            }
        }

        private void Init()
        {
            Metrics.Add(MetricsEnum.Euclid.ToString());
            Metrics.Add(MetricsEnum.Infinit.ToString());
            Metrics.Add(MetricsEnum.Manhattan.ToString());
            Metrics.Add(MetricsEnum.Mahalanobis.ToString());

            ComboBoxMetrics.ItemsSource = Metrics;
        }

        public KmeansDialog()
        {
            InitializeComponent();
            Metrics = new List<string>();

        }

        private void btnGroup_Click(object sender, RoutedEventArgs e)
        {
            MetricsEnum selectedMetric;
            bool isMetricValueConverted = Enum.TryParse((string)ComboBoxMetrics.SelectedValue, out selectedMetric);

            int k = 1;
            bool isKInteger = Int32.TryParse(TextBox_K.Text, out k);

            if (!isMetricValueConverted || !isKInteger)
            {
                MessageBox.Show(" Error parsing metric type");
            }
            else
            {
                var parsedTable = Table.SWDTableToArray();
                var groups = KMeans.KMean(parsedTable, selectedMetric, k);
                var matrix = KMeans.GetDifferenceMatrix(groups, Controller.Table);
                var dialog = new DifferenceMatrixDialog(matrix);
                dialog.Show();
            }
        }

        private void btnOptimalClass_Click(object sender, RoutedEventArgs e)
        {
            MetricsEnum selectedMetric;
            bool isMetricValueConverted = Enum.TryParse((string)ComboBoxMetrics.SelectedValue, out selectedMetric);

            if (!isMetricValueConverted)
            {
                MessageBox.Show(" Error parsing metric type");
            }
            else
            {
                var parsedTable = Table.SWDTableToArray();
                var optimalClasses = KMeans.CalculateOptimalClustersCount(parsedTable, selectedMetric);
                MessageBox.Show(string.Format("Optimal class number = : {0}", optimalClasses));
            }

        }
    }
}
