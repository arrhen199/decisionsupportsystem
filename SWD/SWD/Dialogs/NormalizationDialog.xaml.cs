﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for NormalizationDialog.xaml
    /// </summary>
    public partial class NormalizationDialog : Window
    {
        public bool DeleteColumn { get; set; }

        public NormalizationDialog()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DeleteColumn = CheckBoxDeleteColumn.IsChecked.Value;
            DialogResult = true;
            Close();
        }
    }
}
