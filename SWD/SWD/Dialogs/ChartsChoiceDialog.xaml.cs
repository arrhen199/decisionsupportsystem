﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for ChartsChoiceDialog.xaml
    /// </summary>
    public partial class ChartsChoiceDialog : Window
    {
        public bool Is2D { get; set; }
        public bool ClassChecked { get; set; }
        public string ColumnAxisX { get; set; }
        public string ColumnAxisY { get; set; }
        public string ColumnAxisZ { get; set; }
        public string ColumnClass { get; set; }
        public List<string> ColumnsNumbers { get; set; }
        public List<string> ColumnsStrings { get; set; }
        private ISWDTable _table;
        public ISWDTable Table
        {
            get
            {
                return _table;
            }
            set
            {
                _table = value;
                Init();
            }
        }

        public ChartsChoiceDialog()
        {
            InitializeComponent();
            ColumnsStrings = new List<string>();
            ColumnsNumbers = new List<string>();
        }

        private void Init()
        {
           for(int i = 0; i < Table.GetTable().Columns.Count; i++)
            {
                var colName = Table.GetTable().Columns[i].ColumnName;
                var colType = Table.GetColumnType(i);
                if (colType == "string")
                {
                    ColumnsStrings.Add(colName);
                }
                else
                {
                    ColumnsNumbers.Add(colName);
                }
            }

            ComboBoxAxisX.ItemsSource = ColumnsNumbers;
            ComboBoxAxisY.ItemsSource = ColumnsNumbers;
            ComboBoxAxisZ.ItemsSource = ColumnsNumbers;
            ComboBoxClasses.ItemsSource = ColumnsStrings;
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            ButtonOk.IsEnabled = true;
            Is2D = RadioButton2D.IsChecked.Value;
            CheckBoxClasses.Visibility = Is2D ? Visibility.Visible : Visibility.Collapsed;
            ComboBoxClasses.Visibility = Is2D ? Visibility.Visible : Visibility.Collapsed;
            ComboBoxAxisZ.Visibility = Is2D ? Visibility.Collapsed : Visibility.Visible;
            LabelAxisZ.Visibility = Is2D ? Visibility.Collapsed : Visibility.Visible;

        }

        private void CheckBoxClasses_Click(object sender, RoutedEventArgs e)
        {
            ClassChecked = CheckBoxClasses.IsChecked.Value;
            ComboBoxClasses.IsEnabled = ClassChecked;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            ColumnAxisX = (string)ComboBoxAxisX.SelectedValue;
            ColumnAxisY = (string)ComboBoxAxisY.SelectedValue;
            ColumnAxisZ = (string)ComboBoxAxisZ.SelectedValue;
            ColumnClass = (string)ComboBoxClasses.SelectedValue;
            Close();
        }
    }
}
