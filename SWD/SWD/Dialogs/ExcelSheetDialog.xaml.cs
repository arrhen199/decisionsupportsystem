﻿
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD.Dialogs
{
    /// <summary>
    /// Interaction logic for ExcelSheetDialog.xaml
    /// </summary>
    public partial class ExcelSheetDialog : Window
    {
        public string SheetName { get; set; }
        public ExcelSheetDialog()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            SheetName = (string)ComboBoxSheets.SelectedValue;
            Close();
        }

        internal void SetFilePath(string filePath)
        {
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            excelReader.IsFirstRowAsColumnNames = true;
            DataSet excelResult = excelReader.AsDataSet();

            List<string> sheetNames = new List<string>();
            for(int i=0; i<excelResult.Tables.Count; i++)
            {
                sheetNames.Add(excelResult.Tables[i].TableName);
            }
            ComboBoxSheets.ItemsSource = sheetNames;
            ComboBoxSheets.SelectedIndex = 0;

        }
    }
}
