﻿using Logic;
using Logic.Algorithms;
using Logic.Algorithms.Classifications;
using Logic.Algorithms.Clustering;
using Logic.DataLoader;
using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            String pathdata = Path.Combine(desktopPath, "data_3kl_a.TXT");
            String nerki = Path.Combine(desktopPath, "data_2kl_nerki.TXT");
            String asd = Path.Combine(desktopPath, "data_3kl_b.TXT");
            String multiDim = Path.Combine(desktopPath, "multi_dim.TXT");

            Controller controller = new Controller();

            controller.LoadData(pathdata);
            var c = Separation.GetVectors(controller.Table);

            controller.LoadData(nerki);
            var d = Separation.GetVectors(controller.Table);

            controller.LoadData(asd);
            var dd = Separation.GetVectors(controller.Table);

            controller.LoadData(multiDim);
            var ddd = Separation.GetVectors(controller.Table);

            //String pathIncome = Path.Combine(desktopPath, "INCOME.txt");
            //String pathArrhytmia1 = Path.Combine(desktopPath, "arrhythmia1.txt");
            //String pathArrhytmia2 = Path.Combine(desktopPath, "arrhythmia2.txt");
            //String pathArrhytmia3 = Path.Combine(desktopPath, "arrhythmia3.txt");

            //GenerateOptimalClassesFile(pathIris, Path.Combine(desktopPath, "iris_euclid.txt"), MetricsEnum.Euclid);
            //GenerateOptimalClassesFile(pathIris, Path.Combine(desktopPath, "iris_czebyszew.txt"), MetricsEnum.Infinit);
            //GenerateOptimalClassesFile(pathIris, Path.Combine(desktopPath, "iris_euclid.txt"), MetricsEnum.Manhattan);
            //GenerateOptimalClassesFile(pathIris, Path.Combine(desktopPath, "iris_euclid.txt"), MetricsEnum.Mahalanobis);

            //GenerateOptimalClassesFile(pathIncome, Path.Combine(desktopPath, "income_euclid.txt"), MetricsEnum.Euclid);
            //GenerateOptimalClassesFile(pathIncome, Path.Combine(desktopPath, "income_czebyszew.txt"), MetricsEnum.Infinit);
            //GenerateOptimalClassesFile(pathIncome, Path.Combine(desktopPath, "income_euclid.txt"), MetricsEnum.Manhattan);
            //GenerateOptimalClassesFile(pathIncome, Path.Combine(desktopPath, "income_euclid.txt"), MetricsEnum.Mahalanobis);

            var list = new List<List<double>>();
            list.Add(new List<double> { 5.0, 3.3, 1.4 });
            list.Add(new List<double> { 6.4, 2.8, 5.6 });
            list.Add(new List<double> { 6.5, 2.8, 4.6 });

            var result = Metrics.ComputeCovarianceMatrix(list);
            var dist = Metrics.Mahalanobis(list.ElementAt(0), list.ElementAt(1));
            var euc = Metrics.Euclid(list.ElementAt(0), list.ElementAt(1));

            //var inversed = MatrixHelper.MatrixInverse(result);

            //var first = new List<double> { 5.0, 3.3, 1.4 };

            //var second = new List<double> { 6.4, 2.8, 5.6 };

            //var ress = Metrics.Mahalanobis(first, second);


            //double[,] matrix1 = new double[,] { { 5.0, 6.4, 6.5 }, { 3.3, 2.8, 2.8 }, { 1.4, 5.6, 4.6 } };

            //double[,] matrix2 = new double[,] { { 5.0, 3.3, 1.4 }, { 6.4, 2.8, 5.6 }, { 6.5, 2.8, 4.6 } };

            //var res = MatrixHelper.MultiplyMatrix(matrix1, matrix2);


            //int a = 5;

            var x_ = 2.6;
            var y_ = 21;

            var x = 1;
            var y = 10;

            var x_res = (x * x) - (2 * x * x_) + (x_ * x_);

            var y_res = (y * y) - (2 * y * y_) + (y_ * y_);

            var up_res = (x * y) - (x * y_) - (x_ * y) + (x_ * y_);

            //Task t2 = new Task(() =>
            //{
            //    GenerateOptimalClassesFile(pathArrhytmia2, Path.Combine(desktopPath, "arrhythmia_czebyszew.txt"), MetricsEnum.Infinit);
            //});



            //t2.Start();
            //Console.ReadLine();
        }

        public static void GenerateTextResults(string dataPath, string resultPath, MetricsEnum metric, string decisionClass)
        {
            Controller controller = new Controller();
            controller.LoadData(dataPath);

            using (StreamWriter outputFile = new StreamWriter(resultPath, true))
            {

                for (int i = 0; i < controller.Table.GetTable().Rows.Count - 1; i++)
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    sw.Start();
                    outputFile.WriteLine(String.Format("{0}\t{1}", i + 1, Knn.GetQuality(controller.Table, i + 1, metric, decisionClass)));
                    sw.Stop();
                    Console.WriteLine(i + " " + sw.Elapsed);
                }
            }

        }

        public static void GenerateOptimalClassesFile(string dataPath, string resultPath, MetricsEnum metric)
        {
            Controller controller = new Controller();
            controller.LoadData(dataPath);

            var table = controller.Table.GetTable().Copy();
            table.Columns.RemoveAt(table.Columns.Count - 1);
            var tableWithoutLastColumn = new SWDTable(table);
            var data = tableWithoutLastColumn.SWDTableToArray();

            var dataRows = data.GetLength(0);
            var maxValue = double.MinValue;
            int optimalClustersCount = 0;
            using (StreamWriter outputFile = new StreamWriter(resultPath, true))
            {
                for (int i = 2; i < dataRows - 1; i++)
                {
                    //var value = CalculateDunnIndex(KMean(data, metric, i), data, metric);
                    var value = KMeans.CalculateSilhouette(KMeans.KMean(data, metric, i), data, metric);
                    outputFile.WriteLine(String.Format("{0}\t{1}", i, value));
                    Console.WriteLine(i + " " + metric.ToString());
                    if (maxValue < value)
                    {
                        maxValue = value;
                        optimalClustersCount = i;
                    }
                }
                outputFile.WriteLine("\n\n\nOptimal k = " + optimalClustersCount);
            }
        }


    }
}
