﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface ISWDTable
    {        
        object[] GetRow(int index);
        IEnumerable<double> GetColumn(int index);
        void AddRow(object[] array);
        object GetCell(int row, int col);
        void SetCell(object value, int row, int col);
        void AddColumn(string name);
        void RemoveColumn(string name);
        DataTable GetTable();
        /// <summary>
        /// "int" or "string"
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        string GetColumnType(int col);
        int GetColumnIndex(string name);
        string GetColumnName(int columnIndex);
        double[][] SWDTableToArray();        
    }
}
