﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataLoader
{
    public class DataParser
    {
        private ISWDTable ResultTable { get; set; }
        public DataParser()
        {
            ResultTable = new SWDTable();
        }
        public ISWDTable GetDataFromTxt(string filePath)
        {
            ResultTable = new SWDTable();

            IEnumerable<String> fileLines = File.ReadAllLines(filePath);
            string[] wordsInRow;

            foreach (var line in fileLines)
            {
                wordsInRow = line.Replace("\t", " ").Replace("  ", " ").Replace(";", " ").Replace(" \n", "").Replace(".", ",").Trim().Split(' ');

                if (!wordsInRow[0].StartsWith("#") && line.Length > 0)
                {
                    int numberOfColumns = ResultTable.GetTable().Columns.Count;
                    if (numberOfColumns == wordsInRow.Length)
                        ResultTable.AddRow(wordsInRow);

                    else if (numberOfColumns == 0)
                    {
                        SetColumnNames(wordsInRow, wordsInRow.Length, ResultTable);
                    }
                    else if (numberOfColumns > 0 && wordsInRow.Length > numberOfColumns)
                    {
                        throw new ArgumentOutOfRangeException("Too many characters in a row");
                    }

                }

            }

            return ResultTable;
        }

        public ISWDTable GetDataFromExcel(string filePath, string excelSheet)
        {
            ResultTable = new SWDTable();
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            excelReader.IsFirstRowAsColumnNames = true;
            DataSet excelResult = excelReader.AsDataSet();

            var sheetIndex = 0;
            for (int i = 0; i < excelResult.Tables.Count; i++)
            {
                if (excelResult.Tables[i].TableName == excelSheet)
                {
                    sheetIndex = i;
                    break;
                }
            }
            DataTable table = excelResult.Tables[excelSheet];    //From the first sheet

            List<string> columnNames = new List<string>();
            foreach (DataColumn dataColumn in table.Columns)
            {
                columnNames.Add(dataColumn.ColumnName);
            }

            SetColumnNames(columnNames.ToArray(), table.Columns.Count, ResultTable);

            foreach (DataRow dataRow in table.Rows)
            {
                if (!dataRow.ItemArray[0].ToString().StartsWith("#"))
                    ResultTable.AddRow(dataRow.ItemArray);
            }

            excelReader.Close();


            return ResultTable;

        }

        public string GetDataFromTable()
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataColumn tabColumn in ResultTable.GetTable().Columns)
            {
                sb.Append(String.Format("{0} ", tabColumn.ColumnName));
            }
            sb.AppendLine();
            foreach (DataRow tableRow in ResultTable.GetTable().Rows)
            {
                foreach (var rowItem in tableRow.ItemArray)
                {
                    sb.Append(String.Format("{0} ", rowItem.ToString()));
                }
                sb.AppendLine();
            }

            return sb.ToString().Trim();
        }

        public string GetDataFromBinaryVectors(string[,] vectors)
        {
            StringBuilder sb = new StringBuilder();
            bool isFirstRow = true;

            for (int i = 0; i < vectors.GetUpperBound(0); i++)
            {
                var binaryTable = vectors[i,0].ToArray();
                if (isFirstRow)
                {
                    for (int j = 1; j <= binaryTable.Length; j++)
                    {
                        sb.Append("Col" + j);
                        sb.Append(",");
                    }
                    sb.Append("Class");
                    //sb.AppendLine();
                    isFirstRow = false;
                }
                sb.AppendLine();
                foreach (var binItem in binaryTable)
                {
                    sb.Append(binItem);
                    sb.Append(",");
                }
                sb.Append(vectors[i, 1]);
                
            }

            return sb.ToString();
        }

        private bool ContainsNumbers(string[] firstRow)
        {
            int result = 0;
            foreach (var item in firstRow)
            {
                if (int.TryParse(item, out result))
                    return true;
            }
            return false;
        }

        private void SetColumnNames(string[] colNames, int colNumber, ISWDTable result)
        {
            if (ContainsNumbers(colNames.ToArray()))
            {
                int numberOfColumns = 0;
                for (int i = 1; i <= colNumber; i++)
                {
                    result.AddColumn(String.Format("Column{0}", numberOfColumns + i));
                }
                result.AddRow(colNames);
            }
            else
            {
                foreach (var colName in colNames)
                {
                    result.AddColumn(colName);
                }
            }
        }

    }
}
