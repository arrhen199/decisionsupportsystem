﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataLoader
{
    public class DataLoader
    {
        private string EXCEL_EXTENSION = ".xlsx";
        private string TXT_EXTENSION = ".txt";

        private DataParser Parser;

        public DataLoader()
        {
            Parser = new DataParser();
        }

        public ISWDTable LoadFile(string filePath, string excelSheet)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("File not found.");
            }
            else
            {
                string extension = Path.GetExtension(filePath);

                if (string.Equals(extension, EXCEL_EXTENSION,StringComparison.OrdinalIgnoreCase))
                {
                    return Parser.GetDataFromExcel(filePath, excelSheet);
                }
                else if (string.Equals(extension, TXT_EXTENSION, StringComparison.OrdinalIgnoreCase ))
                {
                    return Parser.GetDataFromTxt(filePath);
                }
                else
                {
                    throw new FormatException("Format not supported.");
                }
            }

        }

        internal void SaveFile(string filePath)
        {
            File.WriteAllText(filePath, Parser.GetDataFromTable());
        }

        public void SaveFile(string filePath, string[,] vectors)
        {
            File.WriteAllText(filePath, Parser.GetDataFromBinaryVectors(vectors));
        }

    }
}
