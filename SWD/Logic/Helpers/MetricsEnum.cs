﻿using System;
using System.Web;

namespace Logic.Helpers
{
    public enum MetricsEnum
    {
        Euclid,
        Manhattan,
        Infinit,
        Mahalanobis
    }
    
}
