﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Controller
    {
        public ISWDTable Table { get; private set; }
        private DataLoader.DataLoader Loader { get; set; }

        public Controller()
        {
            Loader = new DataLoader.DataLoader();
        }

        public void LoadData(string filePath, string excelSheet = "")
        {
            Table = Loader.LoadFile(filePath, excelSheet);
        }

        public void SaveData(string filePath)
        {
            Loader.SaveFile(filePath);
        }
        public void SaveData(string filePath, string[,] vectors)
        {
            Loader.SaveFile(filePath, vectors);
        }
    }
}
