﻿using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class SWDTable : ISWDTable
    {
        private DataTable Table { get; set; }
        private List<String> ColumnNames { get; set; }

        public SWDTable(DataTable dataTable)
        {
            ColumnNames = new List<string>();
            Table = dataTable;
        }

        public SWDTable()
        {
            Table = new DataTable();
            ColumnNames = new List<string>();
        }

        public void AddColumn(string name)
        {
            if (!ColumnNames.Contains(name))
            {
                Table.Columns.Add(name);
                ColumnNames.Add(name);
            }
            else
            {
                throw new InvalidOperationException("Column name already in use!");
            }
        }
        public void RemoveColumn(string name)
        {
            if (ColumnNames.Contains(name))
            {
                Table.Columns.RemoveAt(GetColumnIndex(name));
                ColumnNames.Remove(name);
            }
        }

        public void AddRow(object[] array)
        {
            Table.Rows.Add(array);
        }

        public object GetCell(int row, int col)
        {
            return Table.Rows[row][col];
        }

        public int GetColumnIndex(string name)
        {
            return ColumnNames.IndexOf(name);
        }

        public string GetColumnType(int col)
        {
            if (Table.Rows.Count > 0)
            {
                object data = Table.Rows[0][col];

                int n;
                double d;
                bool isInt = int.TryParse(data.ToString(), out n);
                bool isDouble = double.TryParse(data.ToString(), out d);

                if (isInt)
                {
                    return "int";
                }
                else if (isDouble)
                {
                    return "double";
                }
                else if (data is string)
                {
                    return "string";
                }
            }
            return "unknown";
        }

        public void SetTable(DataTable table)
        {
            this.Table = table.Copy();
        }

        public object[] GetRow(int index)
        {
            return Table.Rows[index].ItemArray;
        }

        public DataTable GetTable()
        {
            return Table;
        }

        public void SetCell(object value, int row, int col)
        {
            Table.Rows[row][col] = value;
        }

        public string GetColumnName(int columnIndex)
        {
            return ColumnNames[columnIndex];
        }

        public IEnumerable<double> GetColumn(int index)
        {
            List<double> values = new List<double>();
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                values.Add(Convert.ToDouble(GetCell(i, index).ToString().Replace('.', ',')));
            }
            return values;
        }

        public double[][] SWDTableToArray()
        {
            int rowsCount = Table.Rows.Count;
            double[][] tableArray = new double[rowsCount][];

            for (int i = 0; i < rowsCount; i++)
            {
                var numbers = new List<double>();
                var items = Table.Rows[i].ItemArray;
                for (int j = 0; j < items.Length; j++)
                {
                    double value = 0;
                    var isConverted = double.TryParse(items[j].ToString(), out value);
                    if (isConverted)
                    {
                        numbers.Add(value);
                    }
                }
                tableArray[i] = numbers.ToArray();
            }
            return tableArray;
        }       

        public SWDTable Copy()
        {
            SWDTable copy = new SWDTable(GetTable().Copy());
            copy.ColumnNames = this.ColumnNames.Clone();

            return copy;
        }

        public void RemoveRow(int index)
        {
            Table.Rows.RemoveAt(index);
        }
    }
}
