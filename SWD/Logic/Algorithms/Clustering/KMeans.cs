﻿using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Clustering
{
    public static class KMeans
    {
        private static double[][] GenerateStartingPoints(double[][] data, int k, MetricsEnum metric)
        {
            Random rnd = new Random();
            int rowsCount = data.GetLength(0);
            int centerPointIndex = rnd.Next(rowsCount);
            double[][] centerPoints = new double[k][];


            if (metric == MetricsEnum.Mahalanobis)
            {
                Metrics.ComputeCovarianceMatrix(data);
            }

            double[] distances = Enumerable.Repeat(Double.MaxValue, rowsCount).ToArray();
            centerPoints[0] = data[centerPointIndex];
            for (int j = 0; j < k - 1; j++)
            {
                for (int i = 0; i < rowsCount; i++)
                {
                    var newDistance = Math.Pow(Metrics.GetDistance(centerPoints[j], data[i], metric), 2);
                    if (newDistance < distances[i])
                    {
                        distances[i] = newDistance;
                    }
                }

                double[] cumulatedDistances = new double[rowsCount];
                cumulatedDistances[0] = distances[0];
                for (int i = 1; i < rowsCount; i++)
                {
                    cumulatedDistances[i] = cumulatedDistances[i - 1] + distances[i];
                }

                double newCenter = rnd.NextDouble() * cumulatedDistances[rowsCount - 1];
                centerPoints[j + 1] = data[rowsCount - 1];
                for (int i = 0; i < rowsCount; i++)
                {
                    if (newCenter < cumulatedDistances[i])
                    {
                        centerPoints[j + 1] = data[i];
                        break;
                    }

                }
            }


            return centerPoints;
        }
        public static Dictionary<int, List<int>> KMean(double[][] data, MetricsEnum metric, int k, int iterations = 1000, double possibleQuantizationError = 0)
        {
            var startingPoints = GenerateStartingPoints(data, k, metric);
            var groups = new Dictionary<int, List<int>>();
            int iterationsCount = 0;
            int rowsCount = data.GetLength(0);
            int startingPointsCount = startingPoints.GetLength(0);

            if (metric == MetricsEnum.Mahalanobis)
            {
                Metrics.ComputeCovarianceMatrix(data);
            }

            double errorPrevious = double.MaxValue;
            double errorCurrent;

            while (iterationsCount <= iterations)
            {
                groups.Clear();
                for (int i = 0; i < rowsCount; i++)
                {
                    double shortestDistance = double.MaxValue;
                    int shortestDistanceIndex = int.MinValue;
                    for (int j = 0; j < startingPointsCount; j++)
                    {
                        var distance = Metrics.GetDistance(data[i], startingPoints[j], metric);
                        if (shortestDistance > distance)// what to do if they are equal
                        {
                            shortestDistance = distance;
                            shortestDistanceIndex = j;
                        }
                    }
                    if (groups.ContainsKey(shortestDistanceIndex))
                    {
                        groups[shortestDistanceIndex].Add(i);
                    }
                    else
                    {
                        groups.Add(shortestDistanceIndex, new List<int> { i });
                    }
                }

                errorCurrent = CalculateQuantizationError(data, startingPoints, groups, metric);
                if ((errorPrevious - errorCurrent) / errorCurrent <= possibleQuantizationError)
                {
                    break;
                }
                else
                {
                    errorPrevious = errorCurrent;
                }

                for (int i = 0; i < k; i++)
                {
                    if (!groups.ContainsKey(i))
                        groups.Add(i, new List<int>());
                }

                startingPoints = RecalculatePoints(data, groups);
                iterationsCount++;
            }

            return groups;
        }

        private static double CalculateQuantizationError(double[][] data, double[][] centers, Dictionary<int, List<int>> groups, MetricsEnum metric)
        {
            double result = 0;
            foreach (var key in groups.Keys)
            {
                double[] centerPoint = centers[key];
                foreach (var index in groups[key])
                {
                    double[] point = data[index];
                    result += Metrics.GetDistance(centerPoint, point, metric);
                }
            }
            result /= data.Length;
            return result;
        }

        private static double[][] RecalculatePoints(double[][] data, Dictionary<int, List<int>> groups)
        {
            int pointsCount = groups.Keys.Count;
            int columnsCount = data[0].Length;
            double[][] result = new double[pointsCount][];


            foreach (var key in groups.Keys)
            {
                var rowIndexes = groups[key];
                if (result[key] == null)
                {
                    result[key] = new double[columnsCount];
                }
                if (rowIndexes.Count == 0)
                    continue;

                foreach (int row in rowIndexes)
                {
                    for (int col = 0; col < columnsCount; col++)
                    {

                        result[key][col] += data[row][col];
                    }
                }

                for (int col = 0; col < columnsCount; col++)
                {
                    result[key][col] /= rowIndexes.Count;
                }
            }

            return result;
        }

        public static double CalculateDunnIndex(Dictionary<int, List<int>> groups, double[][] data, MetricsEnum metric)
        {
            var clustersCenters = RecalculatePoints(data, groups);
            var centersCount = clustersCenters.GetLength(0);

            double maxAvgDist = 0;

            if (metric == MetricsEnum.Mahalanobis)
            {
                Metrics.ComputeCovarianceMatrix(data);
            }

            double avgDist = 0;
            int distancesCount = 0;

            foreach (var values in groups.Values)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    for (int j = 0; j < values.Count; j++)
                    {
                        if (j != i)
                        {
                            distancesCount++;
                            avgDist += Metrics.GetDistance(data[i], data[j], metric);
                        }
                    }
                }
                avgDist = avgDist / distancesCount;
                if (avgDist > maxAvgDist)
                {
                    maxAvgDist = avgDist;
                }
                avgDist = 0;
                distancesCount = 0;
            }

            double minClusterDist = double.MaxValue;

            for (int i = 0; i < centersCount; i++)
            {
                for (int j = i + 1; j < centersCount; j++)
                {
                    var distance = Metrics.GetDistance(clustersCenters[i], clustersCenters[j], metric);
                    if (minClusterDist > distance)
                    {
                        minClusterDist = distance;
                    }
                }
            }

            return minClusterDist / maxAvgDist;

        }

        public static int CalculateOptimalClustersCount(double[][] data, MetricsEnum metric)
        {
            var dataRows = data.GetLength(0);
            var maxValue = double.MinValue;
            int optimalClustersCount = 0;
            double[] values = new double[dataRows];
            for (int i = 2; i < dataRows - 1; i++)
            {
                //var value = CalculateDunnIndex(KMean(data, metric, i), data, metric);
                var value = CalculateSilhouette(KMean(data, metric, i), data, metric);
                values[i] = value;
                if (maxValue < value)
                {
                    maxValue = value;
                    optimalClustersCount = i;
                }
            }
            return optimalClustersCount;
        }



        public static DifferenceMatrix GetDifferenceMatrix(Dictionary<int, List<int>> groups, ISWDTable originalTable)
        {
            Dictionary<String, int> orginalDataDecisionClassValues = new Dictionary<String, int>();
            int columnsCount = originalTable.GetTable().Columns.Count;
            for (int i = 0; i < originalTable.GetTable().Rows.Count; i++)
            {
                String decissionClassValue = (String)originalTable.GetCell(i, columnsCount - 1);
                if (orginalDataDecisionClassValues.ContainsKey(decissionClassValue))
                {
                    orginalDataDecisionClassValues[decissionClassValue]++;
                }
                else
                {
                    orginalDataDecisionClassValues.Add(decissionClassValue, 1);
                }

            }

            DifferenceMatrix result = new DifferenceMatrix(orginalDataDecisionClassValues.Keys.ToList());
            foreach (var values in groups.Values)
            {
                Dictionary<String, int> groupsDecisionClassCounter = new Dictionary<string, int>();
                foreach (var col in orginalDataDecisionClassValues.Keys)
                {
                    groupsDecisionClassCounter.Add(col, 0);
                }

                for (int i = 0; i < values.Count; i++)
                {
                    String classValue = (String)originalTable.GetCell(values[i], columnsCount - 1);
                    groupsDecisionClassCounter[classValue]++;
                }

                int maxValue = 0;
                int maxOccurence = 0;
                foreach (int value in groupsDecisionClassCounter.Values)
                {
                    if (maxValue < value)
                    {
                        maxValue = value;
                        maxOccurence = 1;
                    }
                    else if (value == maxValue)
                    {
                        maxOccurence++;
                    }
                }

                String groupClass = "";
                var possibleGroupClasses = groupsDecisionClassCounter.Where(x => x.Value == maxValue).Select(x => x.Key).ToList();
                if (maxOccurence == 1)
                {
                    groupClass = possibleGroupClasses.First();
                }
                else if (maxOccurence > 1)
                {
                    String lowestTotalName = "";
                    int lowestTotalCount = Int32.MaxValue;
                    foreach (var cl in possibleGroupClasses)
                    {
                        if (orginalDataDecisionClassValues[cl] < lowestTotalCount)
                        {
                            lowestTotalName = cl;
                            lowestTotalCount = orginalDataDecisionClassValues[cl];
                        }
                    }
                    groupClass = lowestTotalName;
                }

                int groupClassIndex = result.Columns.IndexOf(groupClass);
                foreach (var gr in groupsDecisionClassCounter)
                {
                    result.Matrix[result.Columns.IndexOf(gr.Key), groupClassIndex] += gr.Value;
                }

            }

            return result;
        }

        public static double CalculateSilhouette(Dictionary<int, List<int>> groups, double[][] data, MetricsEnum metric)
        {
            double[] silhouetteValues = new double[data.GetLength(0)];
            for (int i = 0; i < groups.Count; i++)
            {
                var group = groups[i];
                for (int x = 0; x < group.Count; x++)
                {
                    //compute a(x)
                    double a = 0;
                    for (int y = 0; y < group.Count; y++)
                    {
                        if (x == y)
                            continue;

                        a += Metrics.GetDistance(data[group[x]], data[group[y]], metric);
                    }
                    if(group.Count > 1)
                        a /= group.Count - 1;


                    double bMin = Double.MaxValue;
                    //compute b(x)
                    for (int j = 0; j < groups.Count; j++)
                    {
                        double b = 0;
                        if (i == j)
                            continue;

                        var otherGroup = groups[j];
                        for(int k=0; k<otherGroup.Count; k++)
                        {
                            b += Metrics.GetDistance(data[group[x]], data[otherGroup[k]], metric);
                        }
                        b /= otherGroup.Count;
                        if (b < bMin)
                            bMin = b;
                    }

                    silhouetteValues[x] = (bMin - a )/ Math.Max(bMin, a);
                }
            }
            return silhouetteValues.Average();
        }
    }
}
