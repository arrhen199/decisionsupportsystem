﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Clustering
{
    public class DifferenceMatrix
    {
        public List<String> Columns { get; set; }
        public int[,] Matrix { get; set; }

        public DifferenceMatrix(List<String> columns)
        {
            Columns = columns;
            Matrix = new int[Columns.Count, Columns.Count];
        }

        public double GetPurityPercent()
        {
            int totalSum = 0;
            int totalCorrect = 0;
            for(int i=0; i < Matrix.GetLength(0); i++)
            {
                for(int j=0; j<Matrix.GetLength(1); j++)
                {
                    totalSum += Matrix[i, j];
                }
                totalCorrect += Matrix[i, i];
            }

            return totalCorrect / (double)totalSum;
        }

    }
}
