﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Classifications
{
    public static class Separation
    {
        public static Dictionary<string, string> VectorToClassValueDict;
        public static string[,] BinaryDataArray;
        public static List<SeparationLine> SepLines;
        public static int removedCount;

        public static string[,] GetBinaryData(ISWDTable data)
        {
            var tableArray = data.SWDTableToArray();
            int rowsCount = tableArray.GetLength(0);
            BinaryDataArray = new string[rowsCount, 2];
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < rowsCount; i++)
            {
                var row = tableArray[i];
                for (int j = 0; j < row.Length - 1; j++)
                {
                    sb.Append(row[j]);
                    sb.Append(" ");
                }
                var vector = GetBinaryVector(sb.ToString());
                BinaryDataArray[i, 0] = vector;
                BinaryDataArray[i, 1] = row[row.Length - 1].ToString();

                sb.Clear();
            }
            return BinaryDataArray;
        }

        public static int GetVectors(ISWDTable data)
        {
            removedCount = 0;
            var tableArray = data.SWDTableToArray();
            int columnsCount = tableArray[0].Length - 1; //last is a class column
            VectorToClassValueDict = new Dictionary<string, string>();

            int vectorSize = 0;

            SepLines = new List<SeparationLine>();

            while (tableArray.Length > 0)
            {
                int rowsCount = tableArray.GetLength(0);
                List<int> indexesToRemove = new List<int>();
                double resLine = 0;
                int lineDim = 0;
                bool isHigher = false;

                for (int i = 0; i < columnsCount; i++)
                {
                    List<int> tmpMaxIndexesToRemove = new List<int>();
                    List<int> tmpMinIndexesToRemove = new List<int>();

                    double maxValue = double.MinValue;
                    double minValue = double.MaxValue;
                    int maxValueIndex = -1;
                    int minValueIndex = -1;

                    for (int j = 0; j < rowsCount; j++)
                    {
                        if (tableArray[j][i] > maxValue)
                        {
                            maxValue = tableArray[j][i];
                            maxValueIndex = j;
                        }

                        if (tableArray[j][i] < minValue)
                        {
                            minValue = tableArray[j][i];
                            minValueIndex = j;
                        }
                    }

                    double diffClassMaxVal = double.MinValue;
                    double diffClassMinVal = double.MaxValue;
                    double maxLineValue = 0;
                    double minLineValue = 0;

                    for (int j = 0; j < rowsCount; j++)
                    {
                        if (tableArray[j][columnsCount] != tableArray[maxValueIndex][columnsCount] && diffClassMaxVal < tableArray[j][i])
                        {
                            diffClassMaxVal = tableArray[j][i];
                            maxLineValue = (maxValue + diffClassMaxVal) / 2;
                        }

                        if (tableArray[j][columnsCount] != tableArray[minValueIndex][columnsCount] && diffClassMinVal > tableArray[j][i])
                        {
                            diffClassMinVal = tableArray[j][i];
                            minLineValue = (minValue + diffClassMinVal) / 2;
                        }
                    }

                    if (diffClassMaxVal == double.MinValue)
                    {
                        //todo 
                        CreateVectors(data, SepLines, columnsCount);
                        return vectorSize;
                    }
                    double minOfMax = double.MaxValue;
                    double maxOfMin = double.MinValue;
                    for (int j = 0; j < rowsCount; j++)
                    {
                        if (tableArray[j][i] > diffClassMaxVal && tableArray[j][i] <= maxValue)
                        {
                            if (minOfMax > tableArray[j][i])
                                minOfMax = tableArray[j][i];
                            tmpMaxIndexesToRemove.Add(j);
                        }

                        if (tableArray[j][i] >= minValue && tableArray[j][i] < diffClassMinVal)
                        {
                            if (maxOfMin < tableArray[j][i])
                                maxOfMin = tableArray[j][i];
                            tmpMinIndexesToRemove.Add(j);
                        }
                    }

                    if (indexesToRemove.Count < tmpMaxIndexesToRemove.Count || indexesToRemove.Count < tmpMinIndexesToRemove.Count)
                    {
                        if (indexesToRemove.Count <= tmpMaxIndexesToRemove.Count)
                        {
                            indexesToRemove = tmpMaxIndexesToRemove;
                            resLine = (diffClassMaxVal + minOfMax) / 2;
                            lineDim = i;
                            isHigher = true;
                        }
                        if (indexesToRemove.Count <= tmpMinIndexesToRemove.Count)
                        {
                            indexesToRemove = tmpMinIndexesToRemove;
                            resLine = (diffClassMinVal + maxOfMin) / 2;
                            lineDim = i;
                            isHigher = false;
                        }

                    }

                    //if (indexesToRemove.Count < tmpMaxIndexesToRemove.Count || indexesToRemove.Count < tmpMinIndexesToRemove.Count)
                    //    indexesToRemove = tmpMaxIndexesToRemove.Count > tmpMinIndexesToRemove.Count ? tmpMaxIndexesToRemove : tmpMinIndexesToRemove;

                }
                if (indexesToRemove.Count == 0)
                {
                    indexesToRemove.Add(0);
                    removedCount++;
                    if (columnsCount < 3)
                    {
                        DataRow rowToDelete = null;
                        Console.WriteLine("Removing point : " + tableArray[0][0] + " : " + tableArray[0][1]);
                        foreach (DataRow row in data.GetTable().Rows)
                        {

                            if (double.Parse((string)row.ItemArray[0]) == tableArray[0][0] && double.Parse((string)row.ItemArray[1]) == tableArray[0][1])
                            {
                                rowToDelete = row;
                            }
                        }
                        data.GetTable().Rows.Remove(rowToDelete);
                    }



                    tableArray = tableArray.Where((el, x) => x != indexesToRemove[0]).ToArray();
                }
                else
                {
                    SepLines.Add(new SeparationLine
                    {
                        ColumnIndex = lineDim,
                        LineValue = resLine,
                        isBiggerClass = isHigher
                    });
                    indexesToRemove = indexesToRemove.OrderByDescending(p => p).ToList();
                    for (int i = 0; i < indexesToRemove.Count; i++)
                    {
                        tableArray = tableArray.Where((el, x) => x != indexesToRemove[i]).ToArray();
                    }
                    vectorSize++;
                }

            }
            return vectorSize;
        }

        public static string GetBinaryVector(string data)
        {
            var stringDataArray = data.Replace("\t", " ").Replace("  ", " ").Replace(";", " ").Replace(" \n", "").Replace(".", ",").Trim().Split(' ');
            List<double> doubleDataArray = new List<double>();
            foreach (var item in stringDataArray)
            {
                doubleDataArray.Add(ObjectToDouble(item));
            }

            string binaryVector = "";
            foreach (var sepLine in SepLines)
            {
                double val = doubleDataArray[sepLine.ColumnIndex];
                bool classified = false;
                if (sepLine.isBiggerClass)
                    classified = val > sepLine.LineValue;
                else
                    classified = val < sepLine.LineValue;

                binaryVector += classified ? "1" : "0";
            }
            return binaryVector;

        }
        public static string GetClass(string binaryVector)
        {
            var res = VectorToClassValueDict.Where(x => x.Key == binaryVector).FirstOrDefault().Value;
            if (res == null)
                return "error";
            else
                return res;
        }

        public static void CreateVectors(ISWDTable data, List<SeparationLine> lines, int decisionColumnIndex)
        {
            int rows = data.GetTable().Rows.Count;
            for (int i = 0; i < rows; i++)
            {
                string binaryVector = "";
                string classVal = data.GetCell(i, decisionColumnIndex).ToString();
                foreach (var sepLine in lines)
                {
                    double val = ObjectToDouble(data.GetCell(i, sepLine.ColumnIndex));
                    bool classified = false;
                    if (sepLine.isBiggerClass)
                        classified = val > sepLine.LineValue;
                    else
                        classified = val < sepLine.LineValue;

                    binaryVector += classified ? "1" : "0";
                }
                if (!VectorToClassValueDict.ContainsKey(binaryVector))
                    VectorToClassValueDict[binaryVector] = classVal;
            }


            //    int rows = data.GetTable().Rows.Count;
            //        for (int i = 0; i<rows; i++)
            //        {
            //            string binaryVector = "";
            //    string classVal = data.GetCell(i, decisionColumnIndex).ToString();
            //            foreach(var sepLine in SeparatingLines)
            //            {
            //                double val = ObjectToDouble(data.GetCell(i, sepLine.ColumnIndex));
            //    bool classified = false;
            //                if (sepLine.AreBiggerClassified)
            //                    classified = val > sepLine.Value;
            //                else
            //                    classified = val<sepLine.Value;

            //                binaryVector += classified? "1" : "0";
            //            }
            //VectorToClassValueDict[binaryVector] = classVal;
        }

        private static double ObjectToDouble(object o)
        {
            double d;
            if (o is IConvertible)
            {
                d = ((IConvertible)o).ToDouble(null);
            }
            else
            {
                d = 0d;
            }
            return d;
        }
    }
}

