﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Classifications
{
    public class SeparationLine
    {
        public int ColumnIndex { get; set; }
        public double LineValue { get; set; }
        public bool isBiggerClass { get; set; }
    }
}
