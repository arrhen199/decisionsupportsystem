﻿using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Classifications
{
    public static class Knn
    {
        public static string Classify(object[] classObject, ISWDTable table, int k, MetricsEnum metric, string decisionClass)  //types??
        {
            var tableRows = table.GetTable().Rows.Count;
            var decisionClassIndex = table.GetColumnIndex(decisionClass);
            double[] distances = new double[tableRows];
            int[] rowNumbers = new int[tableRows];

            var objectList = classObject.ToList();
            objectList.RemoveAt(decisionClassIndex);

            var tableWithoutDecisionClass = table.GetTable().Copy();
            tableWithoutDecisionClass.Columns.RemoveAt(decisionClassIndex);

            if (metric == MetricsEnum.Mahalanobis)
            {
                var data = TableToListOfLists(tableWithoutDecisionClass);
                Metrics.ComputeCovarianceMatrix(data);
            }

            for (int i = 0; i < tableRows; i++)
            {
                rowNumbers[i] = i;
                distances[i] = getDistance(objectList.ToArray(), tableWithoutDecisionClass.Rows[i].ItemArray, metric);

            }

            Array.Sort(distances, rowNumbers);
            Dictionary<string, int> map = new Dictionary<string, int>();
            Dictionary<string, double> distancesMap = new Dictionary<string, double>();

            for (int i = 0; i < k; i++)
            {
                var key = table.GetCell(rowNumbers[i], decisionClassIndex).ToString();
                if (map.ContainsKey(key))
                {
                    map[key]++;
                    distancesMap[key] += distances[i];
                }
                else
                {
                    map.Add(key, 1);
                    distancesMap.Add(key, distances[i]);
                }
            }
            var items = from pair in map
                        orderby pair.Value descending
                        select pair;

            int neighboursCount = items.Max(x=>x.Value);

            var keys = map.Where(x => x.Value == neighboursCount).Select(x => x.Key).ToList();
            var result = string.Empty;
            double resultValue = Double.MinValue;
            foreach (var key in keys)
            {
                if (resultValue < distancesMap[key])
                {
                    resultValue = distancesMap[key];
                    result = key;
                }

            }

            return result;

        }

        private static List<List<double>> TableToListOfLists(DataTable table)
        {
            List<List<double>> data = new List<List<double>>();
            var tableRows = table.Rows.Count;
            for (int i = 0; i < tableRows; i++)
            {
                data.Add(new List<double>());

                for (int j = 0; j < table.Columns.Count; j++)
                {
                    var tableRow = table.Rows[i];
                    string stringValue = tableRow[j].ToString();
                    double value = 0;
                    var isConverted = double.TryParse(stringValue, out value);
                    if (isConverted)
                    {
                        data[i].Add(value);
                    }
                    else
                    {
                        //throw new Exception("Cant parse to double.");
                    }
                }
            }
            return data;
        }

        public static string GetQuality(ISWDTable table, int k, MetricsEnum metric, string decisionClass)
        {
            var tmpTable = new SWDTable();
            tmpTable.SetTable(table.GetTable());
            int correctClassified = 0;

            //DateTime now = DateTime.Now;
            //string newColumnName = decisionClass + "__Classified" + now.ToLongTimeString();
            //table.AddColumn(newColumnName);
            //int newColumnIndex = table.GetColumnIndex(newColumnName);
            int tableRows = table.GetTable().Rows.Count;
            int decisionClassIndex = table.GetColumnIndex(decisionClass);


            for (int i = 0; i < tableRows; i++)
            {
                var delRow = tmpTable.GetTable().Rows[i].ItemArray;
                table.GetTable().Rows.RemoveAt(0);

                //  TODO   WE NEED TO REMOVE ALREADY CLASSIFIED COLUMNS IF THEY ARE NUMERIC

                var classResult = Classify(delRow, table, k, metric, decisionClass);
                var itemClass = delRow[decisionClassIndex].ToString();
                if (string.Equals(classResult,itemClass))
                {
                    correctClassified++;
                }
                table.GetTable().Rows.Add(delRow);
                //table.SetCell(classResult, tableRows - 1, newColumnIndex);
            }

            double quality = (double)correctClassified / (double)tableRows;
            return quality.ToString();
        }

        private static double getDistance(object[] classObject, object[] row, MetricsEnum metric)
        {
            var classObjNumbers = parseToDoubleArray(classObject).ToList();
            var rowObjNumbers = parseToDoubleArray(row).ToList();

            if (rowObjNumbers.Count != classObjNumbers.Count)
            {
                throw new Exception("Objects lengths doesn't match");
            }

            if (metric == MetricsEnum.Euclid)
            {
                return Metrics.Euclid(classObjNumbers, rowObjNumbers);
            }
            else if (metric == MetricsEnum.Infinit)
            {
                return Metrics.Infinit(classObjNumbers, rowObjNumbers);
            }
            else if (metric == MetricsEnum.Manhattan)
            {
                return Metrics.Manhattan(classObjNumbers, rowObjNumbers);
            }
            else
            {
                return Metrics.Mahalanobis(classObjNumbers, rowObjNumbers);
            }
            throw new NotImplementedException();
        }

        private static IEnumerable<double> parseToDoubleArray(object[] dataRow)
        {
            var numbers = new List<double>();
            for (int i = 0; i < dataRow.Length; i++)
            {
                double value = 0;
                var isConverted = double.TryParse(dataRow[i].ToString(), out value);
                if (isConverted)
                {
                    numbers.Add(value);
                }
            }
            return numbers;
        }
    }
}
