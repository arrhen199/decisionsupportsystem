﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Classifications
{
    public class DecisionTreeNode
    {
        public bool IsLeaf { get; set; }
        public String Attribute { get; set; }
        public String Value { get; set; }
        public String ClassValue { get; set; }
        public List<DecisionTreeNode> Children { get; set; }
    }
}
