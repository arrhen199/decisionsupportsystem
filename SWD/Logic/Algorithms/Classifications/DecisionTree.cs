﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms.Classifications
{
    public class DecisionTree
    {
        public static DecisionTreeNode BuildTree(SWDTable tableWithDecisionClass)
        {
            DecisionTreeNode root = new DecisionTreeNode();
            root.Children = BuildTreeRecursively(tableWithDecisionClass, root);
            string ident = "";
            PrintTree(root, ident);
            return root;
        }

        public static void PrintTree(DecisionTreeNode r, string ident)
        {
            //if (r.Children != null)
            //{
            //    foreach (var item in r.Children)
            //    {
            //        if(item.ClassValue ==null)
            //        Console.WriteLine(ident + item.Attribute + " " + item.Value);
            //        else
            //            Console.WriteLine(ident + item.Attribute + " " + item.Value + "  " + item.ClassValue);
            //        //ident += "  ";
            //        PrintTree(item,ident + "  ");
            //    }
            //}
        }

        private static List<DecisionTreeNode> BuildTreeRecursively(SWDTable tableWithDecisionClass, DecisionTreeNode parentNode)
        {
            List<DecisionTreeNode> nodes = new List<DecisionTreeNode>();
            var value = GetClassValueIfDistinct(tableWithDecisionClass);
            if (value != "")    //end
            {
                parentNode.ClassValue = value;
                parentNode.IsLeaf = true;
                return null;
            }
            var attribute = DecisionTree.GetBestAttribute(tableWithDecisionClass);
            var attrTableDictionary = DecisionTree.DivideByAttribute(tableWithDecisionClass, attribute);
            foreach (var key in attrTableDictionary.Keys)
            {
                DecisionTreeNode node = new DecisionTreeNode();
                node.Attribute = attribute;
                node.Value = key;
                node.Children = BuildTreeRecursively(attrTableDictionary[key], node);
                nodes.Add(node);
            }

            return nodes;
        }
        private static string GetBestAttribute(SWDTable tableWithDecisionClass)
        {
            int attributesCount = tableWithDecisionClass.GetTable().Columns.Count - 1;
            int rowsCount = tableWithDecisionClass.GetTable().Rows.Count;
            List<string> decisionValues = new List<string>();
            for (int i = 0; i < rowsCount; i++)
            {
                var val = tableWithDecisionClass.GetCell(i, attributesCount).ToString();
                if (!decisionValues.Contains(val))
                    decisionValues.Add(val);
            }


            double[] entropy = new double[attributesCount];

            for (int i = 0; i < attributesCount; i++)
            {
                List<string> attrValues = new List<string>();
                for (int j = 0; j < rowsCount; j++)
                {
                    var val = tableWithDecisionClass.GetCell(j, i).ToString();
                    if (!attrValues.Contains(val))
                        attrValues.Add(val);
                }

                int[,] freqTable = new int[attrValues.Count, decisionValues.Count];
                for (int j = 0; j < rowsCount; j++)
                {
                    int r = attrValues.IndexOf(tableWithDecisionClass.GetCell(j, i).ToString());
                    int c = decisionValues.IndexOf(tableWithDecisionClass.GetCell(j, attributesCount).ToString());
                    freqTable[r, c]++;
                }
                entropy[i] = GetEntropy(freqTable);
            }


            int index = entropy.ToList().IndexOf(entropy.Min());
            return tableWithDecisionClass.GetColumnName(index);
            //return GetRandomAttribute(tableWithDecisionClass);
        }

        private static double GetEntropy(int[,] freqTable)
        {
            double result = 0;
            int totalSum = 0;
            List<int> rowSum = new List<int>();
            for (int i = 0; i < freqTable.GetLength(0); i++)
            {
                rowSum.Add(0);
                for (int j = 0; j < freqTable.GetLength(1); j++)
                {
                    rowSum[i] += freqTable[i, j];
                }
            }
            totalSum = rowSum.Sum();

            for (int i = 0; i < freqTable.GetLength(0); i++)
            {
                for (int j = 0; j < freqTable.GetLength(1); j++)
                {
                    double p = freqTable[i, j] / (double)rowSum[i];
                    if (p != 0)
                        result -= p * Math.Log(p, 2);
                }
            }

            return result;
        }

        private static String GetRandomAttribute(SWDTable tableWithDecisionClass)
        {
            int index = new Random(DateTime.Now.Millisecond).Next(tableWithDecisionClass.GetTable().Columns.Count - 1);
            return tableWithDecisionClass.GetColumnName(index);
        }

        private static Dictionary<string, SWDTable> DivideByAttribute(SWDTable table, string attr)
        {
            Dictionary<string, SWDTable> result = new Dictionary<string, SWDTable>();

            var tableCopy = table.Copy();
            tableCopy.RemoveColumn(attr);
            int rowsCount = table.GetTable().Rows.Count;
            int columnsCount = table.GetTable().Columns.Count;
            int attrIndex = table.GetColumnIndex(attr);

            for (int i = 0; i < rowsCount; i++)
            {
                string key = table.GetCell(i, attrIndex).ToString();
                if (!result.ContainsKey(key))
                {
                    SWDTable t = new SWDTable();
                    for (int c = 0; c < columnsCount - 1; c++)
                    {
                        t.AddColumn(tableCopy.GetColumnName(c));
                    }
                    result.Add(key, t);
                }
                result[key].AddRow(tableCopy.GetRow(i));
            }

            return result;
        }

        private static string GetClassValueIfDistinct(SWDTable table)
        {
            int rowsCount = table.GetTable().Rows.Count;
            int columnsCount = table.GetTable().Columns.Count;
            int decisionClassIndex = columnsCount - 1;

            if (decisionClassIndex == 0)
            {
                var dictionary = new Dictionary<string, int>();

                for (int i = 0; i < rowsCount; i++)
                {
                    var classVal = table.GetCell(i, decisionClassIndex).ToString();
                    if (dictionary.ContainsKey(classVal))
                    {
                        dictionary[classVal]++;
                    }
                    else
                    {
                        dictionary.Add(classVal, 1);
                    }
                }
                int max = dictionary.Max(x => x.Value);
                return dictionary.First(x => x.Value == max).Key;
            }

            string value = table.GetCell(0, decisionClassIndex).ToString();

            for (int i = 1; i < rowsCount; i++)
            {
                if (table.GetCell(i, decisionClassIndex).ToString() != value)
                {
                    return "";
                }
            }
            return value;
        }

        public static string Classify(SWDTable table, DecisionTreeNode root, string[] objToClassify)
        {
            var node = root;
            while (true)
            {
                if (node.IsLeaf)
                    return node.ClassValue;

                int attrIndex = table.GetColumnIndex(node.Children.First().Attribute);
                var changed = false;
                foreach (var child in node.Children)
                {
                    if (child.Value == objToClassify[attrIndex])
                    {
                        changed = true;
                        node = child;
                        break;
                    }
                }
                if (changed == false)
                    return "";
            }
        }
    }
}
