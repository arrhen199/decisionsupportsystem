﻿using Logic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms
{
    public static class Metrics
    {
        public static double Euclid(List<double> firstObject, List<double> lastObject)
        {
            double result = 0;

            for (int i = 0; i < firstObject.Count; i++)
            {
                result += Math.Pow(lastObject[i] - firstObject[i], 2);
            }
            return Math.Sqrt(result);
        }

        private static double[,] _covarianceMatrix;
        public static double[,] ComputeCovarianceMatrix(List<List<double>> data)
        {
            int rowsCount = data.Count;
            int columnsCount = data.First().Count;
            double[,] result = new double[columnsCount, columnsCount];
            double[] avg = new double[columnsCount];

            for (int i = 0; i < columnsCount; i++)
            {
                for (int j = 0; j < rowsCount; j++)
                {
                    avg[i] += data[j][i];
                }
                avg[i] /= rowsCount;
            }

            for (int i = 0; i < columnsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    //for (int k = 0; k < rowsCount; k++)
                    //{
                    //    result[i, j] += (data[k][i] - avg[i]) * (data[k][j] - avg[j]);
                    //}
                    //result[i, j] /= (rowsCount - 1);


                    if (i == j)
                    {
                        //Variance
                        for (int k = 0; k < rowsCount; k++)
                        {
                            result[i, j] += Math.Pow(data[k][i] - avg[i], 2);
                        }
                        result[i, j] /= (rowsCount - 1);
                    }
                    else
                    {
                        //Covariance
                        for (int k = 0; k < rowsCount; k++)
                        {
                            result[i, j] += data[k][i] * data[k][j];
                        }
                        result[i, j] /= rowsCount;
                        result[i, j] -= avg[i] * avg[j];
                    }
                }
            }
            _covarianceMatrix = result;
            return result;
        }



        public static double Infinit(List<double> firstObject, List<double> lastObject)
        {
            List<double> results = new List<double>();

            for (int i = 0; i < firstObject.Count; i++)
            {
                results.Add(Math.Abs(lastObject[i] - firstObject[i]));
            }
            return results.Max(); ;
        }

        public static double Mahalanobis(List<double> firstObject, List<double> lastObject)
        {
            List<double> columnsSubtracted = new List<double>();
            for (int i = 0; i < firstObject.Count; i++)
            {
                columnsSubtracted.Add(lastObject[i] - firstObject[i]);
            }

            double[,] firstArray = new double[firstObject.Count, 1];
            double[,] secondArray = new double[1, lastObject.Count];

            for (int i = 0; i < firstObject.Count; i++)
            {
                secondArray[0, i] = columnsSubtracted[i];
                firstArray[i, 0] = columnsSubtracted[i];
            }

            var res = secondArray.MultiplyMatrix(_covarianceMatrix.MatrixInverse())
                .MultiplyMatrix(firstArray);

            return Math.Sqrt(res[0, 0]);
        }

        public static double Manhattan(List<double> firstObject, List<double> lastObject)
        {
            double result = 0;

            for (int i = 0; i < firstObject.Count; i++)
            {
                result += Math.Abs(lastObject[i] - firstObject[i]);
            }
            return result;
        }


        public static double Euclid(double[] firstObject, double[] lastObject)
        {
            double result = 0;

            for (int i = 0; i < firstObject.Length; i++)
            {
                result += Math.Pow(lastObject[i] - firstObject[i], 2);
            }
            return Math.Sqrt(result);
        }

        public static double[,] ComputeCovarianceMatrix(double[][] data)
        {
            int rowsCount = data.Length;
            int columnsCount = data.First().Length;
            double[,] result = new double[columnsCount, columnsCount];
            double[] avg = new double[columnsCount];

            for (int i = 0; i < columnsCount; i++)
            {
                for (int j = 0; j < rowsCount; j++)
                {
                    avg[i] += data[j][i];
                }
                avg[i] /= rowsCount;
            }

            for (int i = 0; i < columnsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    //for (int k = 0; k < rowsCount; k++)
                    //{
                    //    result[i, j] += (data[k][i] - avg[i]) * (data[k][j] - avg[j]);
                    //}
                    //result[i, j] /= (rowsCount - 1);


                    if (i == j)
                    {
                        //Variance
                        for (int k = 0; k < rowsCount; k++)
                        {
                            result[i, j] += Math.Pow(data[k][i] - avg[i], 2);
                        }
                        result[i, j] /= (rowsCount - 1);
                    }
                    else
                    {
                        //Covariance
                        for (int k = 0; k < rowsCount; k++)
                        {
                            result[i, j] += data[k][i] * data[k][j];
                        }
                        result[i, j] /= rowsCount;
                        result[i, j] -= avg[i] * avg[j];
                    }
                }
            }
            _covarianceMatrix = result;
            return result;
        }



        public static double Infinit(double[] firstObject, double[] lastObject)
        {
            List<double> results = new List<double>();

            for (int i = 0; i < firstObject.Length; i++)
            {
                results.Add(Math.Abs(lastObject[i] - firstObject[i]));
            }
            return results.Max(); ;
        }

        public static double Mahalanobis(double[] firstObject, double[] lastObject)
        {
            List<double> columnsSubtracted = new List<double>();
            for (int i = 0; i < firstObject.Length; i++)
            {
                columnsSubtracted.Add(lastObject[i] - firstObject[i]);
            }

            double[,] firstArray = new double[firstObject.Length, 1];
            double[,] secondArray = new double[1, lastObject.Length];

            for (int i = 0; i < firstObject.Length; i++)
            {
                secondArray[0, i] = columnsSubtracted[i];
                firstArray[i, 0] = columnsSubtracted[i];
            }

            var res = secondArray.MultiplyMatrix(_covarianceMatrix.MatrixInverse())
                .MultiplyMatrix(firstArray);

            return Math.Sqrt(res[0, 0]);
        }

        public static double Manhattan(double[] firstObject, double[] lastObject)
        {
            double result = 0;

            for (int i = 0; i < firstObject.Length; i++)
            {
                result += Math.Abs(lastObject[i] - firstObject[i]);
            }
            return result;
        }

        public static double GetDistance(double[] dataRow, double[] dataRow2, MetricsEnum metric)
        {
            switch (metric)
            {
                case MetricsEnum.Euclid:
                    return Euclid(dataRow, dataRow2);
                case MetricsEnum.Infinit:
                    return Infinit(dataRow, dataRow2);
                case MetricsEnum.Manhattan:
                    return Manhattan(dataRow, dataRow2);
                case MetricsEnum.Mahalanobis:
                    return Mahalanobis(dataRow, dataRow2);
            }

            throw new Exception("No such metrics implemented.");

        }


    }
}
