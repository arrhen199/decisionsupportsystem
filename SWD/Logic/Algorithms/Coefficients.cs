﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms
{
    public static class Coefficients
    {
        public static double JaccardCoefficient(int[] A, int[] B)
        {
            int[] AnB = A.Intersect(B).ToArray();
            int[] AuB = A.Union(B).ToArray();

            double result = 0;
            if (AuB.Length > 0)
                result = AnB.Length / (double)AuB.Length;

            return result;
        }
        public static double DiceCoefficient(int[] A, int[] B)
        {
            int[] AnB = A.Intersect(B).ToArray();
            int ABLength = A.Length + B.Length;

            double result = 0;
            if (ABLength > 0)
                result = AnB.Length / (double)ABLength;

            return result;
        }
        public static double SimpleCoefficient(int[] A, int[] B, int[] AllPossible)
        {
            int[] notA = AllPossible.Except(A).ToArray();
            int[] notB = AllPossible.Except(B).ToArray();

            int[] AnB = A.Intersect(B).ToArray();
            int[] notAnB = A.Intersect(B).ToArray();

            int allLength = AllPossible.Length;
            double result = 0;
            if (allLength > 0)
                result = (AnB.Length + notAnB.Length)/ (double)allLength;

            return result;
        }
    }
}
