﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Algorithms
{
    public class SimpleAlgorithmsInvoker
    {
        public static void Discretization(ISWDTable table, int columnIndex, int classes, bool deleteColumn)
        {
            var minimum = Double.MaxValue;
            var maximum = Double.MinValue;
            for (int i = 0; i < table.GetTable().Rows.Count; i++)
            {
                var strValue = table.GetCell(i, columnIndex).ToString();
                double value = 0;
                var isConverted = double.TryParse(strValue, out value);

                if (!isConverted)
                {
                    //what to do if value is wrong?
                    throw new Exception("Not numeric value");
                }

                if (value > maximum) maximum = value;
                if (value < minimum) minimum = value;
            }

            string newColumnName = table.GetColumnName(columnIndex) + "__Disc" + classes;
            table.AddColumn(newColumnName);
            int newColumnIndex = table.GetColumnIndex(newColumnName);
            double step = (maximum - minimum) / classes;
            for (int i = 0; i < table.GetTable().Rows.Count; i++)
            {
                var strValue = table.GetCell(i, columnIndex).ToString();
                double value = 0;
                var isConverted = double.TryParse(strValue, out value);
                double newValue = (int)((value - minimum) / step);
                if ((int)(newValue * step) == (int)(maximum - minimum))
                {
                    newValue--;
                }
                table.SetCell(newValue, i, newColumnIndex);
            }

            if (deleteColumn)
            {
                table.RemoveColumn(table.GetColumnName(columnIndex));
            }
        }

        public static void Normalization(ISWDTable table, int columnIndex, bool deleteColumn)
        {
            double sum = 0;
            int rowsCount = table.GetTable().Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                var strValue = table.GetCell(i, columnIndex).ToString();
                double value = 0;
                var isConverted = double.TryParse(strValue, out value);

                if (!isConverted)
                {
                    //what to do if value is wrong?
                    throw new Exception("Not numeric value");
                }
                sum += value;
            }
            double avg = sum / rowsCount;

            double variance = 0;
            for (int i = 0; i < rowsCount; i++)
            {
                var strValue = table.GetCell(i, columnIndex).ToString();
                double value = 0;
                var isConverted = double.TryParse(strValue, out value);

                variance += Math.Pow(value - avg, 2);
            }
            variance = variance / rowsCount;

            double stdDeviation = Math.Sqrt(variance);
            string newColumnName = table.GetColumnName(columnIndex) + "__Norm";
            table.AddColumn(newColumnName);
            int newColumnIndex = table.GetColumnIndex(newColumnName);
            for (int i = 0; i < table.GetTable().Rows.Count; i++)
            {
                var strValue = table.GetCell(i, columnIndex).ToString();
                double value = 0;
                var isConverted = double.TryParse(strValue, out value);
                double newValue = (value - avg) / stdDeviation;

                table.SetCell(newValue, i, newColumnIndex);
            }
            if (deleteColumn)
            {
                table.RemoveColumn(table.GetColumnName(columnIndex));
            }
        }

        public static void StringsToInts(ISWDTable table, int columnIndex, bool orderAlphabetically, bool deleteColumn)
        {            
            string newColumnName = table.GetColumnName(columnIndex) + "__Enum"+ (orderAlphabetically == true ? "Alph" : "Appear");
            table.AddColumn(newColumnName);
            int newColumnIndex = table.GetColumnIndex(newColumnName);

            if (orderAlphabetically)
            {
                List<string> values = new List<string>();
                for (int i = 0; i < table.GetTable().Rows.Count; i++)
                {
                    var strValue = table.GetCell(i, columnIndex).ToString();
                    if (!values.Contains(strValue))
                    {
                        values.Add(strValue);
                    }
                }
                values = values.OrderBy(x => x).ToList();

                for (int i = 0; i < table.GetTable().Rows.Count; i++)
                {
                    var strValue = table.GetCell(i, columnIndex).ToString();
                    table.SetCell(values.IndexOf(strValue), i, newColumnIndex);
                }
            }
            else
            {
                Dictionary<string, int> map = new Dictionary<string, int>();
                int items = 0;
                for (int i = 0; i < table.GetTable().Rows.Count; i++)
                {
                    var strValue = table.GetCell(i, columnIndex).ToString();
                    if (!map.ContainsKey(strValue))
                    {
                        map.Add(strValue, items++);
                    }
                    table.SetCell(map[strValue], i, newColumnIndex);
                }
            }
            if (deleteColumn)
            {
                table.RemoveColumn(table.GetColumnName(columnIndex));
            }

        }
    }
}
